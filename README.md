# UTools
[![](https://img.shields.io/badge/license-Apache%202-4EB1BA.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)
![JDK 1.8](https://img.shields.io/badge/JDK-1.8-green.svg "JDK 1.8")
## 介绍
当前主流的技术栈

## 技术栈列表
类型 | 技术 | 名称 | 链接
-----|------|------|-----
Web框架 | Spring Boot | 容器  | <a href="https://github.com/spring-projects/spring-boot" target="_blank">GitHub</a> \ <a href="https://spring.io/projects/spring-boot" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter" target="_blank">中央仓库</a>
.   | Spring Cloud | 微服务 | <a href="https://github.com/spring-cloud" target="_blank">GitHub</a> \ <a href="http://spring.io/projects/spring-cloud" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter" target="_blank">中央仓库</a>
Netflix 组件   | Eureka | 服务注册与发现 | <a href="https://github.com/Netflix/eureka" target="_blank">GitHub</a> \ <a href="https://cloud.spring.io/spring-cloud-netflix/single/spring-cloud-netflix.html#_service_discovery_eureka_clients" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-eureka" target="_blank">中央仓库</a>
.   | Hystrix | 熔断器 | <a href="https://github.com/spring-cloud/spring-cloud-netflix" target="_blank">GitHub</a> \ <a href="https://cloud.spring.io/spring-cloud-netflix/single/spring-cloud-netflix.html#_service_discovery_eureka_clients" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-hystrix" target="_blank">中央仓库</a>
.   | Zuul | 智能路由 | <a href="https://github.com/Netflix/zuul" target="_blank">GitHub</a> \ <a href="https://cloud.spring.io/spring-cloud-netflix/single/spring-cloud-netflix.html#_service_discovery_eureka_clients" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-netflix-zuul" target="_blank">中央仓库</a>
.   | Ribbon | 负载均衡 | <a href="https://github.com/Netflix/ribbon" target="_blank">GitHub</a> \ <a href="https://cloud.spring.io/spring-cloud-netflix/single/spring-cloud-netflix.html#_service_discovery_eureka_clients" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-netflix-ribbon" target="_blank">中央仓库</a>
.   | Archaius | 统一配置中心 | <a href="https://github.com/Netflix/archaius" target="_blank">GitHub</a> \ <a href="https://cloud.spring.io/spring-cloud-netflix/single/spring-cloud-netflix.html#_service_discovery_eureka_clients" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-netflix-archaius" target="_blank">中央仓库</a>
.   | Actuator | 监控管理 | <a href="https://github.com/spring-projects/spring-boot/tree/master/spring-boot-project/spring-boot-actuator" target="_blank">GitHub</a> \ <a href="https://www.baeldung.com/spring-boot-actuators" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-actuator" target="_blank">中央仓库</a>
Spring Cloud 组件   | Config | 分布式配置中心 | <a href="https://github.com/spring-cloud/spring-cloud-config" target="_blank">GitHub</a> \ <a href="http://spring.io/projects/spring-cloud-config" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-config" target="_blank">中央仓库</a>
.   | Bus | 消息总线 | <a href="https://github.com/spring-cloud/spring-cloud-bus" target="_blank">GitHub</a> \ <a href="http://spring.io/projects/spring-cloud-bus" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-bus" target="_blank">中央仓库</a>
.   | Sleuth | 日志收集 | <a href="https://github.com/spring-cloud/spring-cloud-sleuth" target="_blank">GitHub</a> \ <a href="https://spring.io/projects/spring-cloud-sleuth" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-sleuth" target="_blank">中央仓库</a>
.   | Data Flow | 大数据操作工具 | <a href="https://github.com/spring-cloud/spring-cloud-dataflow" target="_blank">GitHub</a> \ <a href="https://cloud.spring.io/spring-cloud-dataflow/" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-dataflow-core" target="_blank">中央仓库</a>
.   | Consul | 服务注册与发现 | <a href="https://github.com/spring-cloud/spring-cloud-consul" target="_blank">GitHub</a> \ <a href="https://spring.io/projects/spring-cloud-consul" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-consul-core" target="_blank">中央仓库</a>
.   | Zookeeper | 服务注册与发现 | <a href="https://github.com/apache/zookeeper" target="_blank">GitHub</a> \ <a href="https://spring.io/projects/spring-cloud-zookeeper" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-zookeeper-core" target="_blank">中央仓库</a>
.   | Stream | 数据流操作 | <a href="https://github.com/spring-cloud/spring-cloud-stream" target="_blank">GitHub</a> \ <a href="https://cloud.spring.io/spring-cloud-stream/" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-stream" target="_blank">中央仓库</a>
数据库 | MySQL | 关系型数据库 | <a href="https://dev.mysql.com/doc/connector-j/8.0/en/" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/mysql/mysql-connector-java" target="_blank">中央仓库</a>
.   | MongoDB | 分布式文件存储的数据库 | <a href="https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-data-mongodb" target="_blank">中央仓库</a>
.   | Redis | 分布式缓存的数据库 | <a href="https://github.com/antirez/redis" target="_blank" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-data-redis" target="_blank">中央仓库</a>
数据库工具 | Mybatis | ORM框架 | <a href="https://github.com/mybatis/mybatis-3" target="_blank">GitHub</a> \ <a href="http://www.mybatis.org/mybatis-3/zh/index.html" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.mybatis/mybatis" target="_blank">中央仓库</a>
.   | Mybatis-Generator | 代码生成 | <a href="https://github.com/mybatis/generator" target="_blank">GitHub</a>
.   | Mybatis-Generator-Gui | 代码生成 | <a href="https://github.com/zouzg/mybatis-generator-gui" target="_blank">GitHub</a>
.   | Hibernate | ORM框架 | <a href="https://github.com/hibernate/hibernate-orm" target="_blank">GitHub</a> \ <a href="http://hibernate.org/" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.hibernate/hibernate-core" target="_blank">中央仓库</a>
客户端 | Lettuce | Redis客户端 | <a href="https://github.com/lettuce-io/lettuce-core" target="_blank">GitHub</a>
连接池 | Druid | 数据库连接池 | <a href="https://github.com/alibaba/druid" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/com.alibaba/druid" target="_blank">中央仓库</a>
消息中间件 | RabbitMQ | 消息队列 | <a href="https://github.com/rabbitmq/rabbitmq-tutorials" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/com.rabbitmq/amqp-client" target="_blank">中央仓库</a>
.   | ActiveMQ | 消息队列 | <a href="http://activemq.apache.org/" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-activemq" target="_blank">中央仓库</a>
.   | Kafka | 消息队列 | <a href="https://github.com/spring-projects/spring-kafka" target="_blank">GitHub</a> \ <a href="http://spring.io/projects/spring-kafka" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.springframework.kafka/spring-kafka" target="_blank">中央仓库</a>
.   | RocketMQ | 消息队列 | <a href="https://github.com/apache/rocketmq/" target="_blank">GitHub</a> \ <a href="http://rocketmq.apache.org/" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.apache.rocketmq/rocketmq-client" target="_blank">中央仓库</a>
.   | ZeroMQ | 消息队列 | <a href="https://github.com/zeromq/jeromq" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/org.zeromq/jeromq" target="_blank">中央仓库</a>
序列化 | Fastjson | 数据序列化 | <a href="https://github.com/alibaba/fastjson" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/com.alibaba/fastjson" target="_blank">中央仓库
.   | Jackson | 数据序列化 | <a href="https://github.com/FasterXML/jackson-core" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core" target="_blank">中央仓库</a>
.   | Gson | 数据序列化 | <a href="https://github.com/google/gson" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/com.google.code.gson/gson" target="_blank">中央仓库</a>
日志 | Logback | 日志组件 | <a href="https://github.com/qos-ch/logback" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/ch.qos.logback/logback-classic" target="_blank">中央仓库</a>
.   | Slf4j | 日志组件 | <a href="https://github.com/qos-ch/slf4j" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/org.slf4j/slf4j-api" target="_blank">中央仓库</a>
安全框架 | Apache Shiro | 安全 | <a href="https://github.com/apache/shiro" target="_blank">GitHub</a> \ <a href="http://shiro.apache.org/" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.apache.shiro/shiro-spring-boot-web-starter" target="_blank">中央仓库</a>
.   | Security | 安全 | <a href="https://github.com/spring-projects/spring-security" target="_blank">GitHub</a> \ <a href="https://spring.io/projects/spring-security" target="_blank">官网</a> \ <a href="https://github.com/spring-projects/spring-security" target="_blank">中央仓库</a>
安全\校验 | Jwt | Token | <a href="https://github.com/jwtk/jjwt" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt" target="_blank">中央仓库</a>
.   | Kaptcha | 验证码 | <a href="https://github.com/penggle/kaptcha" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt" target="_blank">中央仓库</a>
搜索引擎 | Elastic-Search | 分布式全文搜索 | <a href="https://github.com/elastic/elasticsearch" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/org.elasticsearch/elasticsearch" target="_blank">中央仓库</a>
通讯 | 原生Http | Http通讯 | <a href="https://mvnrepository.com/artifact/org.apache.httpcomponents/httpclient" target="_blank">Http-client 中央仓库</a> \ <a href="https://mvnrepository.com/artifact/org.apache.httpcomponents/httpcore" target="_blank">Http-core 中央仓库</a>
工具 | Pagehelper | Mybatis分页插件 | <a href="https://github.com/pagehelper/Mybatis-PageHelper" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/com.github.pagehelper/pagehelper" target="_blank">中央仓库</a>
.   | Lombok | 简化代码工具 | <a href="https://github.com/rzwitserloot/lombok" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/org.projectlombok/lombok" target="_blank">中央仓库</a>
.   | Joda-Time | 时间/日期库 | <a href="https://github.com/JodaOrg/joda-time" target="_blank">GitHub</a> \ <a href="https://mvnrepository.com/artifact/joda-time/joda-time" target="_blank">中央仓库</a>
.   | Commons-lang3 | 工具类 | <a href="http://commons.apache.org/proper/commons-lang/" target="_blank">官网</a> \ <a href="https://mvnrepository.com/artifact/org.apache.commons/commons-lang3" target="_blank">中央仓库</a>
接口可视化 | Swagger | 接口管理 | <a href="https://mvnrepository.com/artifact/io.springfox" target="_blank">中央仓库</a>
容器化 | Docker | 容器引擎 | <a href="https://hub.docker.com/" target="_blank">Docker Hub</a> \ <a href="https://www.docker.com/" target="_blank">官网</a>
. | kubernetes | 容器编排 | <a href="https://github.com/kubernetes/kubernetes" target="_blank">GitHub</a> \ <a href="https://kubernetes.io/" target="_blank">官网</a>
开发\运维 | Jenkins | 持续集成 | <a href="https://github.com/jenkinsci/jenkins" target="_blank">GitHub</a> \ <a href="https://jenkins.io/" target="_blank">官网</a>
.   | 更多 | xxx | <a href="" target="_blank">GitHub</a> \ <a href="" target="_blank">中央仓库</a>

## 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)