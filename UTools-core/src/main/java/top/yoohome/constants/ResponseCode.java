package top.yoohome.constants;

/**
 * 高可复用响应服务端枚举类
 *
 * @author Johnny Zhang
 */
public enum ResponseCode {
    /**
     * 成功
     */
    SUCCESS("0000", "SUCCESS"),

    /**
     * 失败
     */
    ERROR("9999", "ERROR");

    private final String code;
    private final String msg;

    ResponseCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
