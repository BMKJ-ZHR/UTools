package top.yoohome.util;

import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import top.yoohome.constants.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页对象转换<br>
 *
 * 使用流程：（结合 mybatis 分页插件）<br>
 * （1）PageHelper.startPage(pageNo, pageSize, true);<br>
 * （2）List<TTest> qryList = tTestMapper.selectByExample(example);<br>
 * （3）PageInfo<TTest> pageInfo = new PageInfo<TTest>(qryList);<br>
 * （4）Page<TTestVO> page = PageConvertUtil.map(pageNo, pageSize, pageInfo, TTestVO.class);
 *
 * @author Johnny Zhang
 */
public class PageConvertUtil {

    /**
     * 对象拷贝->List对象<br>
     *
     * 基于 Mybatis
     *
     * @param pageNo 当前页码
     * @param pageSize 每页记录数
     * @param pageInfo Mybatis查询的分页信息
     * @param targetClazz 反射目标对象
     * @return Page<T> 自定义的page对象
     */
    public static <T> Page<T> map(String pageNo, String pageSize, PageInfo<?> pageInfo, Class<T> targetClazz) {
        if (null == pageInfo) {
            int pageNoTemp = getNumByStringToInt(pageNo, 1, 1);
            int pageSizeTemp = getNumByStringToInt(pageSize, 10, 1);
            return new Page<>(pageSizeTemp, pageNoTemp);
        } else {
            int total = Integer.valueOf(String.valueOf(pageInfo.getTotal()));
            Page<T> page = new Page<>(pageInfo.getPageSize(), pageInfo.getPageNum(), pageInfo.getSize(), total);
            List<?> list = pageInfo.getList();
            if (list != null && list.size() > 0) {
                List<T> dataList = ObjectCopyUtil.copy(list, targetClazz);
                page.setDataList(dataList);
            } else {
                page.setDataList(new ArrayList<>());
            }
            return page;
        }
    }

    /**
     * 对当前页码\每页记录数进行数据转换<br>
     *
     * 基于 Mybatis
     *
     * @param str 每页记录数
     * @param returnNum 默认返回数
     * @param limitNum 限制最小返回数
     * @return 整型
     */
    private static int getNumByStringToInt(String str, int returnNum, int limitNum) {
        if (StringUtils.isBlank(str)) {
            return returnNum;
        }
        if (!AuthUtil.authInt(str)) {
            return returnNum;
        }
        int pageSize = Integer.valueOf(str.trim());
        if (pageSize < limitNum) {
            pageSize = returnNum;
        }
        return pageSize;
    }
}
