package top.yoohome.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * 图片相关工具类
 *
 * @author Johnny Zhang
 */
public class ImageUtil {

    /**
     * 图片的像素判断
     *
     * @param file 图片
     * @param imageWidth 图片宽度
     * @param imageHeight 图片高度
     * @return true：上传图片宽度和高度都小于等于规定最大值
     * @throws IOException
     */
    public static boolean checkImageElement(File file, int imageWidth, int imageHeight) throws IOException {
        boolean result = false;
        if (!file.exists()) {
            return false;
        }
        BufferedImage bufferedImage = ImageIO.read(file);
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        if (bufferedImage != null && height == imageHeight && width == imageWidth) {
            result = true;
        }
        return result;
    }

    /**
     * 校验图片比例
     *
     * @param file 图片
     * @param imageWidth 图片宽度
     * @param imageHeight 图片高度
     * @return true：符合要求
     */
    public static boolean checkImageScale(File file, int imageWidth, int imageHeight) throws IOException {
        boolean result = false;
        if (!file.exists()) {
            return false;
        }
        BufferedImage bufferedImage = ImageIO.read(file);
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        if (imageHeight != 0 && height != 0) {
            int scale1 = imageHeight / imageWidth;
            int scale2 = height / width;
            if (scale1 == scale2) {
                result = true;
            }
        }
        return result;
    }

    /**
     * 校验图片的大小<br>
     * 图片最大不能超过5M
     *
     * @param file 图片
     * @param imageSize 图片最大值(KB)
     * @return true：上传图片小于图片的最大值
     */
    public static boolean checkImageSize(File file, Long imageSize) {
        if (!file.exists()) {
            return false;
        }
        // 获取图片大小
        Long size = file.length() / 1024;
        return imageSize != null && size.intValue() <= imageSize;
    }
}
